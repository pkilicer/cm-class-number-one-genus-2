## The file non-max_curves contains curves stored in the following way.
## Each line contains one Galois orbit of moduli points, represented by:
##
## [D,A,B], [n,e,1], [b1,b2,b3,b4], [i1,i2,i3], [a0,a1,...,a5or6]
##
## or
##
## [D,A,B], [n,e,1], [b1,b2,b3,b4], [i1,i2,i3]
##
## Here [D,A,B] is the minimal DAB notation of Kohel, so let K = QQ(c)
## with c^4 + A*c^2 + B = 0.
## Then D is the discriminant of K0 = QQ[x] / (x^2 + A*x + B).
##
## The real quadratic subfield of the reflex field of K is K0r = QQ(a),
## where a^2 + ea + n = 0, we have e in {0, 1}, and e^2-4n is the discriminant of K0r.
##
## There is a non-maximal order O in K with basis
## (sum([bi[j]*c^j for j in [0,1,2,3]]) for i in [1,2,3,4]).
##
## Each of i1, i2, i3, a0, a1, ..., a6 are elements u+v*a of K0r represented by
## a pair [u, v] of rational numbers.
##
## If the line contains "[a0,a1,...,a5or6]", then the curve
## C : y^2 = sum_i a_i*x^i has absolute Igusa invariants i1, i2, i3.
##
## If the line does not contain "[a0,a1,...,a5ora6]", then there
## is no curve defined over K0r corresponding to the moduli point
## (i1,i2,i3), only curves over extensions of K0r.
##
## We conjecture that the curve over QQbar corresponding to the moduli point (i1,i2,i3)
## has endomorphism ring O.
##


## And here is how to load them from a file:

f = open("non-max_curves", 'r')
s = f.readlines()
f.close()


P.<x> = QQ[]

data = []

for l in s:
    l = sage_eval("[" + l + "]")
    DAB = l[0]
    K0r.<a> = NumberField(P(l[1]))
    Q.<y> = K0r[]
    i = [K0r(c) for c in l[3]]
    K = NumberField(x^4+DAB[1]*x^2+DAB[2], 'alpha')
    O = K.order([K(c) for c in l[2]])
    if len(l) == 5:
        f = Q(l[4])
        data.append((DAB, O, i, f))
    else:
        data.append((DAB, O, i))



## Finally, here is how to recompute them and/or do some verifications.

## Put the data files from Bouyer-Streng in a folder using
## mkdir bouyer_streng
## cd bouyer_streng
## wget https://bitbucket.org/mstreng/reduce/raw/ad38afb0fe12282ed89cd4f8c806b1e9cf71d861/data/1a
## wget https://bitbucket.org/mstreng/reduce/raw/ad38afb0fe12282ed89cd4f8c806b1e9cf71d861/data/1b
## wget https://bitbucket.org/mstreng/reduce/raw/ad38afb0fe12282ed89cd4f8c806b1e9cf71d861/data/2b
## wget https://bitbucket.org/mstreng/reduce/raw/ad38afb0fe12282ed89cd4f8c806b1e9cf71d861/data/2c
## cd ..

## Then do the following in SageMath to load them:

BS_list = sum([open("bouyer_streng/" + s, "r").readlines() for s in ["1a", "1b", "2b", "2c"]], [])
DABs_with_duplicates = [eval(l)[0] for l in BS_list]
DABs = []
for DAB in DABs_with_duplicates:
    if not DAB in DABs:
        DABs.append(DAB)


assert(len(DABs) == 83) # 63 of type 2 and 20 of type 1

## Also load the software that we need:

load_attach_mode(True, True)
load("https://bitbucket.org/mstreng/reduce/raw/master/reduce_online.sage")
load("https://bitbucket.org/mstreng/recip/raw/master/recip_online.sage")

count = 0

orders = []


# The following lists all non-maximal orders of CM class number one
# in the loaded non-biquadratic quartic CM fields.

for DAB in DABs:

    print(DAB)
    K = CM_Field(DAB)

    Phi = K.Phi()

    Os = minimal_prime_power_index_orders_of_class_number_one(Phi, output_type='order')

    if len(Os) > 1:
        if DAB == [5,5,5]:
            # The file bisson_streng.sage of recip shows that all orders
            # of CM class number one are contained in one of these orders.
            pass
        else:
            raise NotImplementedError("Just take the intersection, easy to implement, but not needed unless this error is raised.")
    
    for Omin in Os:
        print("  " + str(Os[:3]))
        
        for O in superorders_stable_under_complex_conjugation(Omin[-1]):

            if not O.is_maximal():

                print("    order")
            
                b = is_Omega_equal_Omega_max(O)
                
                if not b:
                    if not DAB == [5,5,5]:
                        raise RuntimeError("With the correct list of fields, this should not happen")
                else:
                    print("    trivial CM class group")
                    
                    p = Omin[0]
                    assert is_prime(p)
                    K = O.number_field()
                    OK = K.maximal_order()
                    for expnt in range(Omin[1]+1):
                        if all([p^expnt * K(b) in O for b in OK.basis()]):
                            F = p^expnt
                            break
                    else:
                        raise RuntimeError("Incorrect F")
                    assert F > 1
                    print("    F = " + str(F))
                    
                    print("    computing abelian surface lattices")
                    pol_id_classes = polarized_ideal_classes(O, F)
                
                    if len(pol_id_classes) > 0:
                
                        print("    number of abelian surfaces:" + str(len(pol_id_classes)))
                        count = count + len(pol_id_classes)
                
                        print(str(count) + " so far")
                    
                        orders.append((O, F, O.index_in(OK)))

assert count == 104 # but counting the ones with Galois group C_4 twice, so this number will go down later

## Then we compute the class polynomials.

def class_polynomials_order_new(O, F, prec, include_period_matrices=False):
    r"""
    This function is as in orders.sage in Recip, except that it uses
    two CM types and gives a result with coefficients in QQ.
    Returns the class polynomials of an order in a primitive quartic CM-field K.
    r"""
    K = O.number_field()
    Phi = K.Phi()
    Phiprime = K.Phiprime()
    Zs = period_matrices(O, F, Phi)
    Zs = Zs + period_matrices(O, F, Phiprime)
    if len(Zs) == 0:
        P = ZZ['X']
        ret = [P(1),P(0),P(0)]
    else:
        i = igusa_invariants_absolute()
        numerical_values = [[j(Z, prec=prec) for Z in Zs] for j in i]
        X = numerical_values[0][0].parent()['X'].gen()
        pols_numerical = ([prod([X-v for v in numerical_values[0]])] +
                        [short_interpolation(numerical_values[0], n) for n in numerical_values[1:]])
        
        ret = [recognize_polynomial(pol_num, QQ) for pol_num in pols_numerical]
    if include_period_matrices:
        return ret, Zs
    else:
        return ret

        
data = []             
        
for d in orders:
    O, e, i = d
    K = O.number_field()
    OK = K.maximal_order()
    DAB = K.minimal_DAB()
    print((DAB, i, e))
    if K.is_galois():
        Phi = K.Phi()
        clpol = class_polynomials_order(O, e, Phi, 1000)
    else:
        clpol = class_polynomials_order_new(O, e, 1000)
    print(clpol)
    data.append((O,DAB,i,e,clpol))


## Some sanity checks, to confirm that we used sufficient precision:

#    The first polynomial factors into distinct (at most) quadratic factors over QQ and linear factors of K0^r:

for d in data: 
    f = d[-1][0].factor()
    for (p,e) in f:
        print(p.degree(), e)
        assert p.degree() <= 2 and e == 1
        if p.degree() == 2:
            [D,A,B] = d[1]
            s = (p.discriminant() / B)
            assert s.is_square()

#    All polynomials have smooth denominators:

for d in data:
    for p in d[-1]:
        for (q, e) in p.denominator().factor():
            assert q <= 139 # The 131 was observed, it would have been fine with 300 as well.
            

#    We found the same number of abelian surfaces as before,
#    except that we counted the ones with cyclic Galois group twice before.

count_cyclic = sum([d[-1][0].degree() for d in data if (d[1][0]/d[1][2]).is_square()])
count_nonGalois = sum([d[-1][0].degree() for d in data if not (d[1][0]/d[1][2]).is_square()])

assert 2*count_cyclic + count_nonGalois == count

#    We now update our count:

count = count_cyclic + count_nonGalois

#   Which is now 70. This proves the number 70 that appears in the proof of Theorem 5.8.

assert count == 70

## Everything below this line is numeric without proof that we have sufficient precision, but is not needed for the proof of Theorem 5.8.

list_of_class_polynomials = [([list(d[0].number_field()(b)) for b in d[0].basis()], d[0].number_field().defining_polynomial(), d[1], d[2], d[3], d[4]) for d in data]

## The following writes the class polynomials to a file:

f = open("non-max_class_polynomials", "w")
f.write(str(list_of_class_polynomials))
f.close()

## This data can be recovered from that file as follows:

P.<x> = QQ[]

f = open("non-max_class_polynomials", "r")

s = f.read()

list_of_class_polynomials = sage_eval(s, {'x':x, 'y':x})

## It is possible to save some time by doing the deduplication now. The code for deduplication is later in this file.

## Next we compute all curves defined over K0r corresponding to our Igusa invariants:


m = []
nonrat = []
for c in list_of_class_polynomials: 
    d = c[2][2].squarefree_part()
    if d % 4 == 1:
        # discriminant d, minimal polynomial x^2 + x + n, where 1-4n = d, so
        n = ZZ((1-d)/4)
        e = 1
    else:
        n = -d
        d = 4*d
        e = 0
        # discriminant d, minimal polynomial x^2 + n
    poly_d = x^2 + e*x + n
    assert poly_d.discriminant() == d
    Kr0.<a> = NumberField(poly_d)
    P.<y> = Kr0[] 
    for (f,ex) in P(c[5][0]).factor(): 
        if ex != 1: 
            raise RuntimeError() 
        if f.degree() > 1: 
            raise RuntimeError() 
        i1 = f.roots()[0][0] # I4I6'/I10
        if i1 in QQ:
            i1 = QQ(i1)
        if c[5][0].degree() == 1:
            der = QQ(c[5][0].derivative())
            i2 = QQ(c[5][1])/der # I2I4^2/I10
            i3 = QQ(c[5][2])/der # I4^5/I10^2
        else:
            der = c[5][0].derivative()(i1)
            i2 = c[5][1](i1)/der # I2I4^2/I10
            i3 = c[5][2](i1)/der # I4^5/I10^2
        if i2 != 0:
            I2 = 1  # A choice of normalisation that works only if I2 is non-zero
            I4 = i3/i2^2*I2^2
            I10 = I2*I4^2/i2
            I6prime = i1*I10/I4
        elif i3 != 0:
            I2 = 0
            # I4^2/I10 = 1 # A choice of normalisation that works only if I4 is non-zero
            I4 = i3
            I10 = I4^2
            I6prime = i1/I4*I10
        else:
            raise ValueError("i1=i2=i3=0 do not specify a unique curve as I6prime is unknown")
        I6 = (I2*I4 - 2*I6prime)/3
        assert I6prime == 1/2*(I2*I4-3*I6)
        assert i1 == I4*I6prime/I10
        assert i2 == I2*I4^2/I10
        assert i3 == I4^5/I10^2
        if I2 == 0:
            f = 4*x^5+40*x^4-40*x^3+20*x^2+20*x+3
            C = HyperellipticCurve(f)
            # This is the one we expect. Let's check if it has the correct invariants.
            a = C.igusa_clebsch_invariants()
            b = (I2,I4,I6,I10)
            u = (a[2]/b[2])/(a[1]/b[1])
            if a != (b[0]*u, b[1]*u^2, b[2]*u^3, b[3]*u^5):
                raise NotImplementedError("HyperellipticCurve_from_invariants not implemented if I2 = 0")
            m.append((c[2],[n,e,1],c[0],[i1,i2,i3],f))
        else:
            M = Mestre_conic([I2,I4,I6,I10])
            # In SageMath 9.1-9.5 don't read the Conic cache because of the bug of trac ticket #33603
            if M.has_rational_point(algorithm='magma', read_cache=False):
                C = HyperellipticCurve_from_invariants([I2,I4,I6,I10], reduced=False, algorithm='magma')
                (f,h) = C.hyperelliptic_polynomials()
                assert h == 0
                m.append((c[2],[n,e,1],c[0],[i1,i2,i3],f))
            else:
                nonrat.append((c[2],[n,e,1],c[0],[list(i) for i in [i1,i2,i3]]))



## We try to reduce the models as much as we can.

## It would be good to automate this a bit more: we need to press Control-C a few times
## to interrupt factorisations that take too long.

failures = []

for i in range(len(m)):
    c = m[i]
    f = c[-1]
    f = reduce_gcd(f)
    try:
        f = reduce_height_with_unit(f)
    except:
        pass
    try:
        f = reduce_discriminant(f, 6, verbose=True)
    except:
        failures.append((i, "disc"))
    try:
        f = stoll_cremona_reduction(f, 6, precision=1000)
    except:
        try:
            f = stoll_cremona_reduction(f, 6, algorithm='magma')
        except:
            failures.append((i, "S-C"))
    try:
        f = reduce_height_with_unit(f)
    except:
        failures.append((i, "unit"))
    m[i] = (c[0],c[1],c[2],c[3],f)

# On a first run with only the non-Galoiscase, we got "disc" once.
# On a rerun with both Galois and non-Galois, we got it 14 times.
# This comes from hard-to-factor number that we interrupted.
#
# On the rerun with both Galois and non-Galois, we got "S-C" 5 times.
# Apparently the current version of the reduce code has difficulty here,
# possibly due to polynomials of degree 5.

## Next, we write the curves to a file:

def write_curves(filename="non-max_curves"):
    s = ""
    for i in range(len(m)):
        c = m[i]
        c = (c[0],c[1],c[2],[list(k) for k in c[3]],[list(k) for k in c[4]])
        s = s + ", ".join([str(d) for d in c]) + "\n"
    for c in nonrat:
        c = (c[0],c[1],c[2],[list(k) for k in c[3]])
        s = s + ", ".join([str(d) for d in c]) + "\n"
    f = open(filename, "w")
    f.write(s)
    f.close()

write_curves()

## And here is how to recover them from the file (roughly the same as on the first lines of this file):

f = open("non-max_curves", 'r')
s = f.readlines()
f.close()


P.<x> = QQ[]

data = []

for l in s:
    l = sage_eval("[" + l + "]")
    DAB = l[0]
    K0r.<a> = NumberField(P(l[1]))
    Q.<y> = K0r[]
    K = CM_Field(DAB)
    O = K.order([K(c) for c in l[2]])
    if len(l[3][0]) == 2:
        i = [K0r(c) for c in l[3]]
        if len(l) == 5:
            f = Q(l[4])
            data.append((DAB, O, i, f))
        else:
            data.append((DAB, O, i))
    else:
        assert all([len(c) == 1 for c in l[3]])
        i = [c[0] for c in l[3]]
        if len(l) == 5:
            assert all([len(c) == 1 for c in l[4]])
            f = [c[0] for c in l[4]]
            f = P(f)
            data.append((DAB, O, i, f))
        else:
            data.append((DAB, O, i))


## If the stored curves are not optimal,
## then this allows us to improve them:

# Recomputing a single curve (using randomized conic solving), this helps if one of the curves had a hard-to-factor discriminant.
# Optionally do it for the ones with a "disc"(riminant) failure.

N = 16
d = data[N]
f = d[3]
C = HyperellipticCurve_from_invariants(HyperellipticCurve(f).igusa_clebsch_invariants(), reduced=False, algorithm='magma')
g = C.hyperelliptic_polynomials()[0]
g = reduce_height_with_unit(g)
g = reduce_discriminant(g, 6, verbose=True)
data[N] = (d[0],d[1],d[2],g)

## Trying stoll-cremona with a possibly different precision and with a different covariant:

for i in range(len(data)):
    d = data[i]
    if len(d) == 4:
        f = d[3]
        try:
            g = stoll_cremona_reduction(f, precision=1000)
        except:
            print(str(i) + " has error")
            g = f
        g = reduce_height_with_unit(g)
        if len(str(g)) > len(str(f)):
            print(str(i) + " would become larger, hence not changed")
        elif g == f:
            print("stayed the same")
        else:
            if len(str(g)) < len(str(f)):
                print(str(i) + " became smaller")
            data[i] = (d[0],d[1],d[2],g)
    
   

for i in range(len(data)):
    d = data[i]
    if len(d) == 4:
        f = d[3]
        try:
            g = stoll_cremona_reduction(f, algorithm='magma', precision=1000)
        except:
            print(str(i) + " has error")
            g = f
        g = reduce_height_with_unit(g)
        if len(str(g)) > len(str(f)):
            print(str(i) + " would become larger, hence not changed")
        elif g == f:
            print("stayed the same")
        else:
            if len(str(g)) < len(str(f)):
                print(str(i) + " became smaller")
            data[i] = (d[0],d[1],d[2],g)


## Out of every pair of conjugate curves, we so far computed both.
## As some of the computations are randomized, the size is better
## for some than for others. We now only keep the best one,
## and we do some sanity checks to see that we really
## have pairs of conjugates.

data_short = []

def compare(d, e):
    """
    Returns 0 if d and e define non-conjugate curves.
    Otherwise returns -1 if str(d) is shorter than str(e) and 1 otherwise.
    """
    (i1,i2,i3) = d[2]
    K0r = i1.parent()
    if not e[2][0] in K0r:
        return 0
    for c in K0r.automorphisms():
        if e[2][0] != c(i1):
            continue
        if d[2] != [c(i) for i in e[2]]:
            raise RuntimeError("Two non-conjugate curves have conjugate i1, either by coincidence, or by a bug.")
        assert len(d) == len(e)
        assert d[0] == e[0]
        # something is wrong with number field comparisons, so we convert things by hand
        Od = d[1]
        Oe = e[1]
        Kd = Od.number_field()
        Ke = Oe.number_field()
        assert Kd.defining_polynomial() == Ke.defining_polynomial()
        assert Od == Kd.order([Kd(list(Ke(b))) for b in Oe.basis()])
        if len(str(d)) < len(str(e)):
            return -1
        return 1
    return 0


for d in data:
    found = False
    for j in range(len(data_short)):
        e = data_short[j]
        c = compare(d, e)
        if found:
            assert c == 0
        if c == -1:
            data_short[j] = d
            found = True
        if c == 1:
            found = True
    if not found:
        data_short.append(d)

assert len(data_short) == 36 # Out of the 70 there were 2 over QQ, so (70-2)/2 + 2 = 36.

## Overwrite the stored data

def nf_elt_to_list(a):
    if a in QQ:
        return [a]
    return list(a)

s = ""
for d in data_short:
    DAB = d[0]
    O = d[1]
    K = O.number_field()
    i = d[2]
    K0r = i[0].parent()
    if K0r is ZZ or K0r is QQ:
        assert DAB == [5,5,5]
        pol = x^2+x-1
        K0r = NumberField(pol,'a')
    else:
        pol = K0r.defining_polynomial()
    c = [DAB, list(pol), [list(K(b)) for b in O.basis()], [list(K0r(j)) for j in i]]
    if len(d) == 4:
        f = d[3]
        c.append([list(K0r(c)) for c in list(f)])
    s = s + ", ".join([str(d) for d in c]) + "\n"
    
f = open("non-max_curves", "w")
f.write(s)
f.close()

## And here is how to recover them from the file (same as on the first lines of this file):

f = open("non-max_curves", 'r')
s = f.readlines()
f.close()


P.<x> = QQ[]

data = []

for l in s:
    l = sage_eval("[" + l + "]")
    DAB = l[0]
    K0r.<a> = NumberField(P(l[1]))
    Q.<y> = K0r[]
    i = [K0r(c) for c in l[3]]
    K = NumberField(x^4+DAB[1]*x^2+DAB[2], 'alpha')
    O = K.order([K(c) for c in l[2]])
    if len(l) == 5:
        f = Q(l[4])
        data.append((DAB, O, i, f))
    else:
        data.append((DAB, O, i))

## Do some independent numerical checks of the data, to verify that there are no bugs or typos or other mix-ups.
## This gives additional evidence for the correctness of our numerically-computed curves.

list_of_i1 = []

list_of_index_and_conductor = []

n_checks = 5
n_breaks = 0

for d in data:
    (i1,i2,i3) = d[2]
    list_of_i1.append(i1)
    if len(d) == 4:
        C = HyperellipticCurve(d[3])
        (I2,I4,I6,I10) = C.igusa_clebsch_invariants()
        I6prime = (I2*I4-3*I6)/2
        assert i1 == I4*I6prime/I10
        assert i2 == I2*I4^2/I10
        assert i3 == I4^5/I10^2
    O = d[1]
    K = O.number_field()
    OK = K.maximal_order()
    i = O.index_in(OK)
    [(p,e)] = factor(i)
    for expnt in range(i):
        if all([p^expnt * K(b) in O for b in OK.basis()]):
            F = p^expnt
            break
    list_of_index_and_conductor.append((i,F,d[0]))
    p = 1000000
    for checks in range(n_checks):
        p = next_prime(p+1)
        while not len(K.ideal(p).factor()) == 4:
            p = next_prime(p+1)
        K0r = d[2][0].parent()
        P = K0r.ideal(p).factor()[0][0]
        [i1, i2, i3] = [GF(p)(P.residue_field()(i)) for i in d[2]]
        if i2 == 0:
            n_breaks = n_breaks+1
            continue
        I2 = 1  # A choice of normalisation that works only if I2 is non-zero
        I4 = i3/i2^2*I2^2
        I10 = I2*I4^2/i2
        I6prime = i1*I10/I4
        I6 = (I2*I4 - 2*I6prime)/3
        C = HyperellipticCurve_from_invariants([I2,I4,I6,I10])
        abs_trace = abs(p + 1 - C.count_points()[0])
        primes = [P for (P,e) in K.ideal(p).factor()]
        assert len(primes) == 4
        # The following set does not take CM types into account, so this set is slightly too big, but that is not a problem
        # Also, it is not guaranteed to yield a generator with pi*pibar in QQ,
        # so a failure of this test could occur even if the curves are correct (**).
        potential_frobs = flatten([[(primes[i]*primes[j]).gens_reduced()[0] for j in range(i)] for i in range(4)])
        if d[0] == [5,5,5]:
            potential_frobs = [pi*u for pi in potential_frobs for u in K.roots_of_unity()]
        potential_frobs = [pi for pi in potential_frobs if pi.norm() == p^2 and pi in d[1]]
        potential_abs_traces = [abs(pi.trace()) for pi in potential_frobs]
        print(abs_trace, potential_abs_traces)
        assert abs_trace in potential_abs_traces # could still fail because of (**) above. In that case, give a more complete list of potential_frobs.
    
## The special code for i2=0 is only used for one curve:
assert n_breaks == n_checks

## All curves have different values of Norm(i1) (except if they are conjugate):

list_of_i1_norm = [i1.norm() for i1 in list_of_i1]                                                                                                        
list_of_i1_norm.sort()       
assert all([list_of_i1_norm[i] != list_of_i1_norm[i+1] for i in range(len(list_of_i1_norm)-1)])

## Here is the index of the order and the least F such that F*OK is contained in O:

for (i, F, DAB) in list_of_index_and_conductor:
    if DAB == [5,5,5]:
        assert (i,F) in [(4,2), (8,2), (16,4), (9,3), (5,5)]
    else:
        assert (i,F) in [(2,2), (4,2), (8,4)]

## There are 15 different fields for which we get curves:

assert len(list(Set([(d[0][0],d[0][1],d[0][2]) for d in data]))) == 15

## There are roughly 21 different orders. Orders in C_4 fields that are not stable under Galois are probably counted twice in this count.

assert len(orders) == 21

## A double-check that our curves are really different from the curves with CM by the maximal order.

for max in BS_list:
    max = sage_eval("["+max+"]")
    if len(max) == 3 and len(max[1][0]) == 1:
        # Type a
        i1 = QQ(max[1][0][0])
    else:
        assert len(max) == 4
        P.<x> = QQ[]
        pol = P(max[1])
        K0r = NumberField(pol, 'a')
        i1 = K0r(max[2][0])
    print(i1.norm())
    assert not i1.norm() in list_of_i1_norm        

