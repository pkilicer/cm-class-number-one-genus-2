"""
Reference: arXiv:1511.04869
"""

"""

This file computes all cyclic PQ1 fields as defined in the reference above. It is tested with SageMath version 9.1
and ReCip version 3.2.1.

It was written for 32 parallel processes. If you want to use a different number of computer cores, change the line
"parallel(32)" below accordingly.

Then do the following in SageMath::

    sage: load_attach_mode(True, True)               # Optional, to get detailed debugging information
    sage: load_attach_path('/home/p285020/genus2') # Replace this by the location of this file
    sage: attach("enumerate_C4_PQ1_fields.sage")     # Load this file
    sage: load("https://bitbucket.org/mstreng/recip/raw/master/recip_online.sage") # Load ReCip. This only works if you
                                                     # built SageMath with an ssl library. Otherwise, download ReCip
                                                     # and load it from there.
    sage: from recip import *                        # This is an alternative to the line above, but only works
                                                     # if you downloaded ReCip and installed it properly.
    sage: load("recip.sage")                         # This is an alternative to the lines above, but only works
                                                     # if you downloaded ReCip and have added its folder to the
                                                     # load_attach_path.

	
	sage: o = open('OUTPUT_cyclic_PQ1_fields.sage','w')  
	sage: o.write('These are cyclic PQ1 fields. \nThe output consists of tuples [K, p, q, [rs], hF, hK/hF, Kr].\n\n')
	sage: o.close()
	
	sage: bound = 2*10^5
	sage: time main(bound)


When we ran this code we got the following output about the time. 

CPU times: user 1h 44min 6s, sys: 10.1 s, total: 1h 44min 16s
Wall time: 1h 44min 16s


"""


def are_all_nonsplit(l, s):
    """
    Tests whether all primes in the list s are non-split (that is, inert or ramified) in Q(sqrt(l)).
    
    INPUT:
    
     - A tuple (l, s), where l is a squarefree integer bigger than 1 and s is a list of primes.
    
    OUTPUT:
    
     - True if and only if all primes in the list s are inert or ramified in Q(sqrt(l))

    """
    for r in s:
        if r == 2:
            if l % 8 == 1:
                return False
        elif legendre_symbol(l, r) == 1:
            return False
    return True

    
def good_prime(p,r): 
    #checks if the prime number r is different from the prime number p and inerts in Q(sqrt(p)). 
    if r != p and are_all_nonsplit(p, [r]):
        return True 
       
            
            
            
def main(bound):
    """
    
    Runs through all primes up to the given integer "bound" and for each prime p smaller than "bound"
    forms lists [p,[1]], [p,[r1]], [p,[r1, r2]], ... with k = < 5, where ri's are primes which are 
    inert in $Q(sqrt{p})$. Then sends these strings to be evaluated in function "find_all_cyclic_PQ1_fields_given_primes()".
   
    INPUT:
    
    '' bound '' -- an integer
	
	NOTE: We proved in Theorem 4.8 that 0<k<6 in the paper.
	NOTE: We proved in Theorem 4.8 that 0<k<6 in the paper.
    
    """
    Bp = bound 
    for p in primes(2, Bp): 
        if p % 4 != 3:
            find_all_cyclic_PQ1_fields_given_primes(p, [1], bound)
            B1 = ceil(Bp/p) # bound for r1
            for r1 in primes(2, B1):
                if good_prime(p, r1): 
                    find_all_cyclic_PQ1_fields_given_primes(p, [r1], bound)
                    B2 = ceil(B1/r1) # bound for r2
                    for r2 in primes(r1+1, B2): # r2 > r1 
                        if good_prime(p, r2): 
                            find_all_cyclic_PQ1_fields_given_primes(p, [r1, r2], bound)
                            B3 = ceil(B2/r2) # bound for r3
                            for r3 in primes(r2+1, B3): # r3 > r2 > r1 
                                if good_prime(p, r3): 
                                    find_all_cyclic_PQ1_fields_given_primes(p, [r1, r2, r3], bound)
                                    B4 = ceil(B3/r3) # bound for r4
                                    for r4 in primes(r3+1,B4): # r4 > r3 > r2 > r1 
                                        if good_prime(p, r4): 
                                            find_all_cyclic_PQ1_fields_given_primes(p, [r1, r2, r3, r4], bound)
                                            B5 = ceil(B4/r4) # bound for r5
                                            for r5 in primes(r4+1,B5): # r5 > r4 > r3 > r2 > r1 
                                                if good_prime(p, r5):
                                                    find_all_cyclic_PQ1_fields_given_primes(p, [r1, r2, r3, r4, r5], bound)
    
def find_all_cyclic_PQ1_fields_given_primes(p, ListOf_rs, N):    
    
    """
	
	For a given [p, [1], N] or [p, [r1, ..., rk], N], where p, r1, ..., rk are primes, 
	this function tests if the correspoding cyclic CM field K is a PQ1 field, and if so, writes it a file.
    
    This is Step 3 in Algortihm 4 in the paper.
    
    INPUT:
    
    '' p '' -- a prime p
    '' ListOf_rs '' -- a list of distinct primes which are different from p 
    '' N '' -- an integer
    
    *********
    
    Here, DAB is the [D, A, B] representation of K; fK is the conductor of K; hF is the class number of F; hK is the class number of K.
     

    NOTE: we proved in Proposition 4.8 that 0<k<6.
    
	OUTPUT:

    No output, but information is written to a file in the form [DAB, fK, p, ListOf_rs, hF, hK/hF].
   
       
    """
    
    if p*prod(ListOf_rs[0:])> N:
        raise RuntimeError("There is a mistake with the bounds of ri's")
    (p, K, DAB, F, dF) = find_cyclic_CM_field_given_pqr(p, ListOf_rs[0:])
    if test_CM_cl_nr_one_with_bach_bound(p, K, dF):
        if test_CM_cl_nr_one_with_class_group(K,p):
            fK = K.conductor()
            hF = F.class_number()
            hK = K.class_number()
            output = [DAB, fK, p, ListOf_rs, hF, hK/hF]
            o = open('OUTPUT_cyclic_quartic_PQ_1.sage','a')  
            o.write(str(output))
            o.write(',\n\n') 
            o.close()

def find_cyclic_CM_field_given_pqr(p, ListOf_rs):

    """
    Constructs a cyclic CM-field $K$ such that
	(1) K_plus = Q(sqrt(p)), the quadratic subfield of field $K$;
	(2) K = Q(sqrt(-eps*s_1\cdots s_u*sqrt(p))) where eps is a fundamental unit such that eps*sqrt(p)>0 
	and s_i are in list_of_si.
	
	This is Step 2 in Algortihm 4 which follows from Proposition 4.2. 

    INPUT:
    
    '' p '' -- a prime p
    '' ListOf_rs '' -- a list of distinct primes which are different from p 
    
    OUTPUT: 
    
    '' p '' - the given prime $p$
    '' F '' - the quadratic field $Q(\sqrt{p})$
    '' K '' - the cyclic quartic CM field constructed with 'p, ListOf_rs'
    '' DAB '' - the [D, A, B] representation of $K$
    '' dF '' - the discriminant of $F$  
       
    """
    F.<a> = QuadraticField(p)
    prod_rs = prod(ListOf_rs)
    epsilon = F.units()[0] # fundamental unit
    o=epsilon*F(p).sqrt()
    if o.is_totally_positive():
        z=-prod_rs*o
    elif (-o).is_totally_positive():
        z=prod_rs*o
    R.<t> = PolynomialRing(F)
    dF = F.discriminant()
    DAB = DAB_to_minimal([dF, -z.trace(), z.norm()])
    K = CM_Field(DAB)
    return p, K, DAB, F, dF

def test_CM_cl_nr_one_with_bach_bound(p, K, dF):

    """
    Test under GRH whether K has CM class number one.
    The output True means that either K has CM class number one or GRH is false.
    The output False means that K does not have CM class number one.

    Uses splitting of small primes as well as Weil Q-number enumeration.

    This is Step 3 in Algorithm 3. 
    
    INPUT:
    
     - ''p'' -- a prime number
     - ''K'' -- a cyclic quartic CM field
     - ''dF'' -- discriminant of F the intermediate field of K.
    
    OUTPUT:
    
     - Returns true if and only if $K$ is a PQ1-field.  (This does not depend on Phi by Lemma 2.4)
	 
	METHOD: 

     - First check whether any small primes split completely (see Lemma 3.29 and Step 3 of Algorithm 3), which is
       a really quick step.
     - Then test whether all primes below the Bach bound are trivial in the CM class group, using enumeration of
       Weil Q-numbers, where Q is the norm of the prime.
       
    """
    poly = K.polynomial()
    dK = K.discriminant()
    splitting_bound = (dK/dF^2)^(1/2)/4
    if check_splitting_condition(poly, splitting_bound): # This is Lemma 3.29. 
	
        """
        Instead of computing class group of the CM field K
        to check whether it satisfies  I_0(Phi^r)/P_K = Cl_K,
        we use Bach bound (assuming GRH), and eliminate if the prime ideals
        with norm less than the Bach bound is not principal under type norm.  
        """  
        Phis = K.CM_types()
        Phi = Phis[0]
        Phir = Phi.reflex() 
        Kr = Phir.domain()
        sqrtp = K(p).sqrt()
        
        bach_bound = int(K.bach_bound()) 
        for l in prime_range(bach_bound): 
            for l_1 in Kr.factor(l):   
                l_1_norm = l_1[0].norm()
                if l_1_norm < bach_bound: 
                    C = Phir.type_norm(l_1[0]) 
                    if not is_generated_by_a_Weil_number_enum(K, p, l_1_norm, C, sqrtp):
                        return False
        return True
    return False

    
def check_splitting_condition(poly, splitting_bound): 
    """

    Tests whether K/Q has no completely split primes with norm less than or equal to
    splitting_bound.
	
	This is Step 2 in Algorithm 3 which follows from Lemma 3.29. 
    
    INPUT: 
    
    ''poly'' -- a monic polynomial in Z[x]
    ''splitting_bound'' -- an integer


    OUTPUT:

    - Returns True if there are no completely split primes in K/Q with norm
    less than or equal to splitting_bound, and False if there is such a prime.

    """
    
    for f in prime_range(splitting_bound): 
        R.<x> = PolynomialRing(GF(f))
        poly_over_finite = R(poly)
        if len(poly_over_finite.factor()) == 4:
            return False
    return True      
                 
                 
                 

def is_generated_by_a_Weil_number_enum(K, p, Q, I, sqrtp):
    """
    Checks if the ideal "I" is generated by a Q-Weil number in K, using Weil-number enumeration.
    This is Algorithm 2 of the paper for the special case d=p. 

    INPUT:
    - ''K'' -- a non-biquadratic quartic CM field
    - ''p'' -- a rational prime such that Q(sqrt(p)) is the intermediate field of K 
    - ''Q'' -- a power of a prime number such that N_{K/\Q}(I) = Q
    - ''I'' -- an ideal of O_K
    - ''sqrtp'' -- square root of p, where K_plus = Q(sqrt{p}) \subset K.

    METHOD:

    Enumerate all Weil numbers alpha in K by enumerating all possible relative traces beta = alpha + alphabar in K_plus.
    This is slow for large values of 'Q', but scales very well with the discriminant of K.
    For a version that is fast for large values of 'Q' (but relies on class group computation, hence only
    works for small discriminants), see what happens inside the function test_CM_cl_nr_one_with_class_group.
    """
    # First, we check whether the ideal I is generated by the Weil number sqrt(Q).
    # This is a common case, so doing this check first speeds up the algorithm.

    if is_generated_by_sqrtQ(K, Q, p, I, sqrtp):
        return True

    # Then we check whether the ideal I is generated by another weil q-number alpha.
    # Let beta = alpha + alphabar.
    # Write beta = ( a + b*sqrt(p) ) / 2.
    # NOTE that a and b in this code are 2*a and 2*b of the paper

    aa = ZZ(floor(4*sqrt(Q)))  # Recall that this a is 2*a of the paper, hence the bound is twice as large as in the paper
    for a in sxrange(aa+1):
        b_2 = floor((4*sqrt(Q)-abs(a))/sqrt(p))
        #sys.stdout.flush()
        for b in sxrange(-b_2, b_2+1):
            if is_integral(a, b, p): # This is an optional check to speed things up a bit more: if (a+b*sqrt(p))/2 is not integral, then that would contradict integrality of alpha.
                if is_Weil_number_generator(K, Q, a, b, p, I, sqrtp):
                    return True
    return False



def is_integral(a, b, p):

    """
    Return true if and only if (a+b*sqrt(p))/2 is integral
    
    INPUT:
    - An integer valued triple (a, b, p)
    
    OUTPUT:
    - True if and only if (a+b*sqrt(p))/2 is integral
    
    """
    if p == 2:
        if a % 2 == 0 and b % 2 == 0:
            return True
    if p % 4 == 1:
        if a % 2 == b % 2:
            return True


def is_generated_by_sqrtQ(K, Q, p, I, sqrtp):
    """
    
    Checks if the ideal "I" is generated by "sqrt(Q)".
    
    INPUT:
    
    - '' K '' -- a quartic CM field K containing K_plus = Q(sqrt(p))
    - '' Q '' -- a power of a prime 
    - '' p '' -- a prime number 
    - '' I '' -- an ideal of O_K
    - '' sqrtp '' -- square root of p
    
    
    OUTPUT:
    
    Returns true if and only if the ideal "I" is generated by "sqrt(Q)".
    
    """
    # Just some background:
    # In the notation of is_generated_by_a_Weil_number_enum, with beta = (a+b*sqrtp)/2,
    # this is the case of beta=0 i.e. the +/-sqrt(Q) case.
    # The weil Q-number is +/- sqrt(Q), which is in K iff Q is a square in ZZ or sqrt(q) \in K.
    # in other words, we test if q equals p
    if Q.is_square():
        # our Weil Q-number is +/- sqrt(Q) and is in ZZ
        return I == K.ideal(Q.sqrt())
    else:
        # Here is a very specific test for non-biquadratic quartic CM fields.
        # We have Q = s^e for some prime s and some e dividing the order of the Galois group and e <= [K : Q],
        # so for quartic CM fields we get e in {1, 2, 4}.
        # As Q is not a square, we get e = 1, so Q is s itself.
        # So to test whether sqrt(Q) is in K, we only have to test whether s is equal to p.
        # A test for more general CM fields would be easy as well:
        # just do K(Q).is_square() and take alpha = K(Q).sqrt()
        if Q != p:
            return False
        # sqrtp = sqrt(p) = sqrt(Q) = +/- our weil q-number
        return I == K.ideal(sqrtp)
    
    
    
def is_Weil_number_generator(K, Q, a, b, p, I, sqrtp):
    """
    Tests whether the given ideal "I" is generated by a Weil-Q number alpha in K
    with alpha + alphabar = (a+b*sqrtp)/2.
    
    INPUT:
    
    - ''K'' -- a quartic CM field containing K_plus=Q(sqrt(p)), where p is a prime number
    - ''Q'' -- a power of a prime l
    - ''a, b'' -- integers such that (a+b*sqrtp)/2 is an algebraic integer.
    - ''p'' -- a prime number
    - ''I'' -- an ideal of OO_K
    - ''sqrtp'' -- square root of p

    OUTPUT:
    
    Returns true if and only if a root of the polynomial f(x) = x^2 + (a+b*sqrtp)/2 * x + Q is a
    generator of the ideal "I".
    """
    assert is_integral(a, b, p)
        
    beta = (a+b*sqrtp)/2  
    
    #g = t^4-a*t^3+a_1*t^2-Q*a*t+(Q)^2
    #g = t^2 - (beta)*t + Q
    #roots of g are Weil q-numbers iff (either beta=0 or beta^2-4*q is totally negative)
    
    #if not (beta^2-4*q).is totally negative: # (by explicitly writing out)

    disc = K(beta^2 - 4*Q)
    if not disc.is_square():
        return False
        
    for s in [-1, 1]:
        alpha = (-beta + s*sqrt(beta^2-4*Q)) / 2
        if I == K.ideal(alpha):
            return True
    return False
                    


def test_CM_cl_nr_one_with_class_group(K, p):

    """
    Checks whether the given cyclic quartic CM field is a PQ1-field.
    (In other words, checks whether I_0(Phir)/P_{Kr} = Cl_{Kr}.)

    This function uses the class group of K, hence is only fast for fields K of small discriminant.
    We use it only after first convincing ourselves in other ways whether that the answer will be 'yes'.

    This is Steps 4 and 5 in Algorithm 3. 

    INPUT: 

    '' K '' - non-cyclic quartic CM field K

    OUTPUT: returns True if K is a PQ1 field

    """
    sqrtp = K(p).sqrt()
    Phis = K.CM_types()
    Phi = Phis[0]
    Phir = Phi.reflex() 
    Kr = Phir.domain()
    sqrtp = K(p).sqrt()
    clKr = Kr.class_group()
    gens = clKr.gens()
    for I in gens:
        I_ideal = I.representative_prime() 
        Inorm = I_ideal.norm()
        C = Phir.type_norm(I_ideal)
        if not is_generated_by_a_Weil_number_enum(K, p, Inorm, C, sqrtp):
            return False
    return True

 




           
            