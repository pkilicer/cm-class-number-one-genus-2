"""
Reference: arXiv:1511.04869
"""


"""

This file computes all non-Galois PQ1 fields as defined in the reference above. It is tested with SageMath version 9.1
and ReCip version 3.2.1.

It was written for 32 parallel processes. If you want to use a different number of computer cores, change the line
"parallel(32)" below accordingly.

Then do the following in SageMath::

    sage: load_attach_mode(True, True)               # Optional, to get detailed debugging information
    sage: load_attach_path('/home/p285020/genus2/') # Replace this by the location of this file
    sage: attach("enumerate_D4_PQ1_fields.sage")     # Load this file
    sage: load("https://bitbucket.org/mstreng/recip/raw/master/recip_online.sage") # Load ReCip. This only works if you
                                                     # built SageMath with an ssl library. Otherwise, download ReCip
                                                     # and load it from there.
    sage: from recip import *                        # This is an alternative to the line above, but only works
                                                     # if you downloaded ReCip and installed it properly.
    sage: load("recip.sage")                         # This is an alternative to the lines above, but only works
                                                     # if you downloaded ReCip and have added its folder to the
                                                     # load_attach_path.
	
	sage: o = open('OUTPUT_non_Galois_PQ1_fields.sage','w')  
	sage: o.write('These are non-Galois PQ1 fields. \nThe output consists of tuples [DAB, p, q, list_of_si, hK_plus, hK/hK_plus, DABr].\n\n')
	sage: o.close()
	
	sage: s = all_p_q_pairs()                   # This step takes about 29 seconds.
    sage: len(s)
    496920
    sage: PQ1_D4_fields = list(find_PQ1_fields_from_list_of_p_and_q_parallel(s))


When we ran this code with 32 parallel sessions, we got the following output about the time. 

CPU times: user 6.95 s, sys: 4min 17s, total: 4min 23s
Wall time: 2h 11min 10s

"""

bound_for_p_times_q = 55 * 10^5 # This is the 5.5 * 10^6 from Corollary 3.27.
bound_for_p = (bound_for_p_times_q / 2).ceil() # q >= 2


# B[u] is the number B_u of Corollary 3.27.
# so prod(s_i) \leq max(sqrt(B[u]/(pq)), 222^2/(pq)).
B = [5.5*10^6, 3.3*10^7, 1.875*10^8, 1.05*10^9, 5.75*10^9, 2.3*10^10]

# Now to bound s_i.
Bs = [max([B[u]/prod(primes_first_n(u)[i:u])^2 for u in range(i, 6)]) for i in range(6)]
# For i = 1, ..., 5 we have
#    prod_{k=1}^u s_k \leq max(sqrt(B[u]/(pq)), 222^2/(pq))
# so s_i \leq max(sqrt(B[u]/(pq)), 222^2/(pq)) / prod_{k\not=i} s_k
#        \leq max(sqrt(Bs[u]/(pq)), 222^2/(pq)) / prod_j=1^{i-1} s_j as s_{i+1}...s_{u} <= p_{i+1}...p_{u}.



assert Bs[1:] == B[1:] # So Bs turns out to be just B.

# Here is what B and Bs look like:
assert B  == [5500000, 33000000, 187500000, 1050000000, 5750000000, 23000000000]
assert Bs[1:] ==      [33000000, 187500000, 1050000000, 5750000000, 23000000000]


def bound_prod_si(p_times_q, u):
    """
    Returns an upper bound on the product s1...su, given p*q.
    For the proof, see Corollary 3.27.
    """
    return ceil(max([sqrt(B[u]/p_times_q), 222^2/p_times_q]))


def bound_si(p_times_q, i, earlier_s):
    """
    Returns an upper bound bound on s_i, given p*q and earlier_s = s1*s2*...*s_{i-1}.
    For the proof, see the comments surrounding the introduction of the list Bs above.
    """
    # By the argument about Bs above, the following are the same:
    return ceil(max([sqrt(Bs[i]/p_times_q), 222^2/p_times_q]) / earlier_s)
    #return bound_prod_si(p_times_q, i) / earlier_s



def are_all_nonsplit(l, s):
    """
    Tests whether all primes in the list s are non-split (that is, inert or ramified) in Q(sqrt(l)).
    
    INPUT:
    
     - A tuple (l, s), where l is a squarefree integer bigger than 1 and s is a list of primes.
    
    OUTPUT:
    
     - True if and only if all primes in the list s are inert or ramified in Q(sqrt(l))

    """
    for r in s:
        if r == 2:
            if l % 8 == 1:
                return False
        elif legendre_symbol(l, r) == 1:
            return False
    return True


def split_primes(p, q):
    """
    Returns True if and only if p splits in Q(sqrt(q)) and q splits in Q(sqrt(p)).

    INPUT:

     - A pair of prime numbers (p, q).

    """
    if p == 2:
        if q % 8 != 1:
            return False  # 2 does not split in Q(sqrtq)
        if legendre_symbol(2, q) != 1:
            return False  # q does not split in Q(sqrt2)
        return True
    if q == 2:
        return split_primes(q, p)
    # Now p and q are both odd.
    return legendre_symbol(p, q) == 1 and legendre_symbol(q, p) == 1


def good_pair(p, q):
    """
    Given two prime numbers p and q, check whether they satisfy some necessary conditions for the construction
    of a PQ1 field. This is Proposition 3.22-(i).

    INPUT:

     - Prime numbers p and q.

    OUTPUT:

     - True if p and q are distinct, q is not 3 mod 4, p splits in Q(sqrt(q)) and q splits in Q(sqrt(p)).
       False otherwise.
    """
    if p == q or q % 4 == 3:
        return False
    return split_primes(p, q)


def good_prime(p, q, r):
    """
    Given three prime numbers p, q and r = s_i, check whether they satisfy some necessary conditions for the
    construction of a PQ1 field. This is Proposition 3.22-(ii).

    INPUT:

     - Prime numbers p, q and r.

    OUTPUT:

     - True if r is inert in Q(sqrt(p)) and Q(sqrt(q)). False otherwise.
    """
    if r == p or r == q:  # quickly checkable condition
        return False
    return are_all_nonsplit(p, [r]) and are_all_nonsplit(q, [r])


def all_p_q_pairs():
    """
    Returns the list of all pairs (p, q) of primes with p*q <= bound_for_p_times_q satisfying the conditions
    of good_pair(p, q).
    """
    list_p_q = []
    for p in primes(bound_for_p):
        for q in primes(ceil(bound_for_p_times_q/p)):
            if good_pair(p,q):
                list_p_q.append((p, q))
    return list_p_q    


def find_PQ1_fields_from_list_of_p_and_q_parallel(L):
    """
    Given a list L of pairs of primes (p, q), finds all non-Galois PQ1 fields with K_plus = Q(sqrt(p)) and
    Kr_plus = Q(sqrt(q)) and writes them to a file.

    Use this function as explained in the documentation at the top of this file.
    """
    size = len(L)
    how_many_chunks = 10000
    chunk_size = ceil(size/how_many_chunks)
    return find_PQ1_fields_from_list_of_p_and_q_main([L[j * chunk_size : (j+1)*chunk_size] for j in range(how_many_chunks)])


@parallel(32) 
def find_PQ1_fields_from_list_of_p_and_q_main(list):
    """
    Use this function via find_PQ1_fields_from_list_of_p_and_q_parallel, and see the documentation of that function for
    what this does.

    Internally, this function goes through the following steps.
    Runs through all prime pairs (p,q) in a given "list" and forms the sublists [p,[1]], [p,[r1]], [p,[r1, r2]], ... with k = < 5,
    and then checks if rk are inert in Q(sqrt(p)) and Q(sqrt(q)). Then
    sends these strings to be evaluated in function "check_nonnormal_PQ1_field_for_given_primes()".
      
    INPUT:
    
    '' list '' - a list of (p,q) pairs. 
    
    NOTE: We proved in Theorem 3.25 that 0<k<6 in the paper.
   
    """
    for (p,q) in list:
        check_nonnormal_PQ1_field_for_given_primes(p, q, [])
        B1 = bound_si(p*q, 1, 1)
        for r1 in primes(2,B1):
            if good_prime(p, q, r1):
                check_nonnormal_PQ1_field_for_given_primes(p, q, [r1])
                B2 = bound_si(p*q, 2, r1)
                for r2 in primes(r1+1,B2):
                    if good_prime(p,q,r2):
                        check_nonnormal_PQ1_field_for_given_primes(p, q, [r1, r2])
                        B3 = bound_si(p*q, 3, r1*r2)
                        for r3 in primes(r2+1,B3):
                            if good_prime(p,q,r3):
                                check_nonnormal_PQ1_field_for_given_primes(p, q, [r1, r2, r3])
                                B4 = bound_si(p*q, 4, r1*r2*r3)
                                for r4 in primes(r3+1,B4):
                                    if good_prime(p,q,r4):
                                        check_nonnormal_PQ1_field_for_given_primes(p, q, [r1, r2, r3, r4])
                                        B5 = bound_si(p*q, 5, r1*r2*r3*r4)
                                        for r5 in primes(r4+1,B5):
                                            if good_prime(p,q,r5):
                                                check_nonnormal_PQ1_field_for_given_primes(p, q, [r1, r2, r3, r4, r5])                                           


def check_nonnormal_PQ1_field_for_given_primes(p, q, list_of_si):
    """
    For given p, q, and list_of_si = [s_1, ..., s_u], this function tests if the
    correspoding non-normal CM field K is a PQ1 field, and if so, writes it a file.
    
    This is Steps 3 and 4 in Algortihm 3 in the paper.

    INPUT:
    
    '' p '' and '' q '' -- a pair of primes p, q satisfying the conditions of good_pair(p, q)
    '' list_of_si '' -- a (possibly empty) list of distinct primes which are different from p and q and inert in
                        Q(sqrt(p)) and Q(sqrt(q)).

    OUTPUT:

    No output, but information is written to a file in the form [DAB, p, q, list_of_si, hK_plus, hK/hK_plus, DABr].
    
       
    """
    (Kr, poly_reflex, Phir, DABr, K, K_plus) = non_normal_CM_field_given_pqr(p, q, list_of_si)
    if test_CM_cl_nr_one_with_bach_bound(p, Kr, poly_reflex, Phir, K, K_plus):
        if test_CM_cl_nr_one_with_class_group(K, p, Kr, Phir):
            DAB = CM_Field(K).minimal_DAB() # from the Recip package
            hK_plus = K_plus.class_number()
            hK = K.class_number()
            output = [DAB, p, q, list_of_si, hK_plus, hK/hK_plus, DABr]
            print("Field found:") 
            print(output)
            o = open('OUTPUT_non_Galois_PQ1_fields.sage','a')  
            o.write(str(output))
            o.write(',\n\n') 
            o.close()


def non_normal_CM_field_given_pqr(p, q, list_of_si):
    """
       
    Constructs a non-normal quartic CM field Kr such that 
    
    (1) Kr_plus = Q(sqrt(q)), the quadratic subfield of field $Kr$;
    (2) K_plus = Q(sqrt(p)), the quadratic subfield of the reflex $K$;
    (3) Kr = Q(sqrt(-pi*s_1\cdots s_u)) where pi is a totally positive generator of a 
    power Pp^n of a prime ideal Pp (Pp^n is a principal ideal) over p in Kr_plus and s_i are in list_of_si.
    
    This is Algortihm 1 in the paper which follows from Proposition 3.22. 

    INPUT:

     - ''p, q'' --  prime numbers such that $p$ splits in Kr_plus = Q(sqrt(q)) and q splits in K_plus = Q(sqrt(p)) and q is not 3 mod 4.
     - ''list_of_si'' -- a list of distinct primes which are different from p and q and inert in K_plus and Kr_plus.

    OUTPUT:

    A tuple, consisting of:

     - ''Kr'' - the non-biquadratic quartic CM field of the form Q(sqrt(-pi*s_1\cdots s_u)) described above.
     - ''poly_reflex'' -- the minimal polynomial of Kr
     - ''Phir'' - a CM type of Kr
     - ''DABr'' - the minimal [D, A, B] representation of Kr
     - ''K'' - the reflex field of (Kr, Phir)
     - ''K_plus'' -- K_plus=Q(sqrt(p)) (isomorphic to the quadratic subfield of K).
    """
    T.<x> = PolynomialRing(QQ)
    Kr_plus.<a> = QuadraticField(q)
    K_plus.<w> = QuadraticField(p)
    beta = Kr_plus.primes_above(p)[0]
    C = Kr_plus.class_group()
    n = C(beta).order()
    if n % 2 == 0:
        raise RuntimeError("This can't happen. Q(sqrt(q)) has narrow class group of odd order.")
    J = beta^n
    (pi,) = J.gens_reduced()
    epsilon = Kr_plus.units()[0]  # fundamental unit
    o = pi * epsilon
    prod_rs = prod(list_of_si)
    if pi.is_totally_positive():
        z = -prod_rs * pi
    elif (-pi).is_totally_positive():
        z = prod_rs * pi
    elif o.is_totally_positive():
        z = -prod_rs * o
    elif (-o).is_totally_positive():
        z = prod_rs * o
    else:
        raise RuntimeError("This can't happen. Q(sqrt(q)) has narrow class group of odd order and epsilon should be a fundamental unit.")
    poly_reflex = z.minpoly()(x^2)   # sqrt(z) is a root of this polynomial
    A = poly_reflex[2]
    B = poly_reflex[0]
    assert poly_reflex[4] == 1
    # assert A in ZZ and B in ZZ # these will be automatically tested later
    D = q if q % 4 in [0, 1] else 4 * q
    DABr = DAB_to_minimal([D,A,B]) 
    Kr = CM_Field(DABr)
    Phis = Kr.CM_types()
    Phir = Phis[0]
    Psi = Phir.reflex()      
    K = Psi.domain()
    return Kr, poly_reflex, Phir, DABr, K, K_plus


def test_CM_cl_nr_one_with_bach_bound(p, Kr, poly_reflex, Phir, K, K_plus):
    """
    Test under GRH whether K has CM class number one.
    The output True means that either K has CM class number one or GRH is false.
    The output False means that K does not have CM class number one.

    Uses splitting of small primes as well as Weil Q-number enumeration.

    This is Step 3 in Algorithm 3. 
    
    INPUT:

     - ''p'' -- the prime number p such that the real quadratic subfield of the reflex field of Kr is isomorphic to Q(sqrt(p)).
     - ''Kr'' - a non-Galois quartic CM field  
     - ''poly_reflex'' -- the defining polynomial of Kr
     - ''Phir'' - a CM type of Kr
     - ''K'' - the reflex of (Kr ,Phir)
     - ''K_plus'' -- the quadratic subfield of K; K_plus=Q(sqrt(p)). 
    
    OUTPUT:
    
     - See above.

    METHOD: 

     - First check whether any small primes split completely (see Lemma 3.29 and Step 3 of Algorithm 3), which is
       a really quick step.
     - Then test whether all primes below the Bach bound are trivial in the CM class group, using enumeration of
       Weil Q-numbers, where Q is the norm of the prime.
    """
    dK = K.discriminant()
    dK_plus = K_plus.discriminant()
    splitting_bound = ceil(sqrt(dK)/(4*dK_plus)) # bound given in Lemma 3.29 in the paper.
    
    if not check_splitting_condition(Kr, poly_reflex, splitting_bound):
        # By Lemma 3.29, the field K is not PQ1 
        return False
        
    sqrtp = K(p).sqrt()
    bach_bound = floor(Kr.bach_bound()) 
    for l in prime_range(bach_bound + 1): 
        for l_1 in Kr.factor(l):
            norm_l_1 = l_1[0].norm()
            if norm_l_1 <= bach_bound:
                C = Phir.type_norm(l_1[0])
                if not is_generated_by_a_Weil_number_enum(K, p, norm_l_1, C, sqrtp):
                    return False
    return True



def check_splitting_condition(Kr, poly_reflex, splitting_bound): 
    """
    
    Tests whether Kr/Q has no completely split primes with norm less than or equal to
    splitting_bound.
    
    This is Step 2 in Algorithm 3 which follows from Lemma 3.29. 

    INPUT:
    
    ''Kr'' -- a number field field
    ''poly_reflex'' -- the defining polynomial of Kr
    ''splitting_bound'' -- an integer

    OUTPUT:

    - Returns True if there are no completely split primes in Kr/Q with norm
    less than or equal to splitting_bound, and False if there is such a prime.
    
    NOTES:

        If a prime l splits completely in the reflex field Kr of a PQ1-field K, then l
        is larger than or equal to splitting_bound = (dK/dK_plus^2)^(1/2)/4.

        In the cyclic case, we have Kr = K so the input is just the CM field $K$ itself.
        In the non-normal case, the input is the reflex field Kr.
    
    """
    
    for l in primes(splitting_bound):
        # We could test directly whether len(Kr.factor(l)) == 4, but the following is faster.
        R.<x> = PolynomialRing(GF(l))
        poly_over_finite = R(poly_reflex)
        factors = poly_over_finite.factor()
        if len(factors) == 4:
            # The prime over l is totally split.
            return False
        if all([e == 1 for (f, e) in factors]):
            # Less than 4 factors, all of multiplicity 1, so not totally split.
            pass
        elif len(Kr.factor(l)) == 4: # factors does not give enough information, so we use the number field object.
            # Totally split.
            return False
        else:
            # Not totally split.
            pass
    return True


def is_generated_by_a_Weil_number_enum(K, p, Q, I, sqrtp):
    """
    Checks if the ideal "I" is generated by a Q-Weil number in K, using Weil-number enumeration.
    This is Algorithm 2 of the paper for the special case d=p. 

    INPUT:
    - ''K'' -- a non-biquadratic quartic CM field
    - ''p'' -- a rational prime such that Q(sqrt(p)) is the intermediate field of K 
    - ''Q'' -- a power of a prime number such that N_{K/\Q}(I) = Q
    - ''I'' -- an ideal of O_K
    - ''sqrtp'' -- square root of p, where K_plus = Q(sqrt{p}) \subset K.

    METHOD:

    Enumerate all Weil numbers alpha in K by enumerating all possible relative traces beta = alpha + alphabar in K_plus.
    This is slow for large values of 'Q', but scales very well with the discriminant of K.
    For a version that is fast for large values of 'Q' (but relies on class group computation, hence only
    works for small discriminants), see what happens inside the function test_CM_cl_nr_one_with_class_group.
    """
    # First, we check whether the ideal I is generated by the Weil number sqrt(Q).
    # This is a common case, so doing this check first speeds up the algorithm.

    if is_generated_by_sqrtQ(K, Q, p, I, sqrtp):
        return True

    # Then we check whether the ideal I is generated by another weil q-number alpha.
    # Let beta = alpha + alphabar.
    # Write beta = ( a + b*sqrt(p) ) / 2.
    # NOTE that a and b in this code are 2*a and 2*b of the paper

    aa = ZZ(floor(4*sqrt(Q)))  # Recall that this a is 2*a of the paper, hence the bound is twice as large as in the paper
    for a in sxrange(aa+1):
        b_2 = floor((4*sqrt(Q)-abs(a))/sqrt(p))
        #sys.stdout.flush()
        for b in sxrange(-b_2, b_2+1):
            if is_integral(a, b, p): # This is an optional check to speed things up a bit more: if (a+b*sqrt(p))/2 is not integral, then that would contradict integrality of alpha.
                if is_Weil_number_generator(K, Q, a, b, p, I, sqrtp):
                    return True
    return False


def is_integral(a, b, p):
    """
    Checks if (a+b*sqrt(p))/2 is an algebraic integer.
    
    INPUT:
    - An integer valued triple (a, b, p) where p is prime.
    
    OUTPUT:
    - True if and only if (a+b*sqrt(p))/2 is integral in Q(sqrt(p))
    
    """
    if p % 4 == 3 or p == 2:
        if a % 2 == 0 and b % 2 == 0:
            return True
    if p % 4 == 1:
        if a % 2 == b % 2:
            return True


def is_generated_by_sqrtQ(K, Q, p, I, sqrtp):
    """
    
    Checks if the ideal "I" is generated by "sqrt(Q)".
    
    INPUT:
    
    - '' K '' -- a quartic CM field K containing K_plus = Q(sqrt(p))
    - '' Q '' -- a power of a prime 
    - '' p '' -- a prime number 
    - '' I '' -- an ideal of O_K
    - '' sqrtp '' -- square root of p
    
    
    OUTPUT:
    
    Returns true if and only if the ideal "I" is generated by "sqrt(Q)".
    
    """
    # Just some background:
    # In the notation of is_generated_by_a_Weil_number_enum, with beta = (a+b*sqrtp)/2,
    # this is the case of beta=0 i.e. the +/-sqrt(Q) case.
    # The weil Q-number is +/- sqrt(Q), which is in K iff Q is a square in ZZ or sqrt(q) \in K.
    # in other words, we test if q equals p
    if Q.is_square():
        # our Weil Q-number is +/- sqrt(Q) and is in ZZ
        return I == K.ideal(Q.sqrt())
    else:
        # Here is a very specific test for non-biquadratic quartic CM fields.
        # We have Q = s^e for some prime s and some e dividing the order of the Galois group and e <= [K : Q],
        # so for quartic CM fields we get e in {1, 2, 4}.
        # As Q is not a square, we get e = 1, so Q is s itself.
        # So to test whether sqrt(Q) is in K, we only have to test whether s is equal to p.
        # A test for more general CM fields would be easy as well:
        # just do K(Q).is_square() and take alpha = K(Q).sqrt()
        if Q != p:
            return False
        # sqrtp = sqrt(p) = sqrt(Q) = +/- our weil q-number
        return I == K.ideal(sqrtp)
    

def is_Weil_number_generator(K, Q, a, b, p, I, sqrtp):
    """
    Tests whether the given ideal "I" is generated by a Weil-Q number alpha in K
    with alpha + alphabar = (a+b*sqrtp)/2.
    
    INPUT:
    
    - ''K'' -- a quartic CM field containing K_plus=Q(sqrt(p)), where p is a prime number
    - ''Q'' -- a power of a prime l
    - ''a, b'' -- integers such that (a+b*sqrtp)/2 is an algebraic integer.
    - ''p'' -- a prime number
    - ''I'' -- an ideal of OO_K
    - ''sqrtp'' -- square root of p

    OUTPUT:
    
    Returns true if and only if a root of the polynomial f(x) = x^2 + (a+b*sqrtp)/2 * x + Q is a
    generator of the ideal "I".
    """
    assert is_integral(a, b, p)
        
    beta = (a+b*sqrtp)/2  
    
    #g = t^4-a*t^3+a_1*t^2-Q*a*t+(Q)^2
    #g = t^2 - (beta)*t + Q
    #roots of g are Weil q-numbers iff (either beta=0 or beta^2-4*q is totally negative)
    
    #if not (beta^2-4*q).is totally negative: # (by explicitly writing out)

    disc = K(beta^2 - 4*Q)
    if not disc.is_square():
        return False
        
    for s in [-1, 1]:
        alpha = (-beta + s*sqrt(beta^2-4*Q)) / 2
        if I == K.ideal(alpha):
            return True
    return False


def test_CM_cl_nr_one_with_class_group(K, p, Kr, Phir):
    """
    Checks whether the given non-Galois quartic CM field is a PQ1-field.
    (In other words, checks whether I_0(Phir)/P_{Kr} = Cl_{Kr}.)

    This function uses the class group of K, hence is only fast for fields K of small discriminant.
    We use it only after first convincing ourselves in other ways whether that the answer will be 'yes'.

    This is Steps 4 and 5 in Algorithm 3. 

    INPUT: 

    '' Kr '' - non-biquadratic quartic CM field
    '' Phir '' - is a CM type of Kr
    '' K '' - the reflex field of (Kr ,Phir)

    OUTPUT: returns True if K is a PQ1 field

    """
    sqrtp = K(p).sqrt()
    clKr = Kr.class_group()
    gens = clKr.gens()
    for I in gens:
        I_ideal = I.representative_prime() 
        Inorm = I_ideal.norm()
        C = Phir.type_norm(I_ideal)
        if not is_generated_by_a_Weil_number_enum(K, p, Inorm, C, sqrtp):
            return False
    return True
